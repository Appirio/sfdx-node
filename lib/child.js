process.on("message", function onMessage(message) {
  if (message.cmd !== "SFDX_PARALLEL_init") {
    return;
  }
  if (onMessage.initialized) {
    return;
  }

  // Should only be initialized once.
  onMessage.initialized = true;

  // Remove the message listener so that the process can exit normally.
  process.removeListener("message", onMessage);

  const { commandPath, flags, opts } = message.args;

  const sendSuccess = (resolvedValue) => {
    process.send({
      type: 'resolved',
      value: resolvedValue,
    });
  };
  const sendFailure = (rejectedValue) => {
    // Ensure that the error is thrown as an array of plain JS objects, in format => { message: "some error", stack: "stack trace for the error" }
    const errors = require('./process-errors');
    process.send({
      type: 'rejected',
      value: errors.processAllErrors(rejectedValue),
    });
  };

  try {
    const _ = require('lodash');
    const sfdx = require('../index');
    const command = _.get(sfdx, commandPath);
    const value = command(flags, opts);
    if (value !== undefined && value !== null && typeof value.then === 'function') {
      value.then((result) => {
        sendSuccess(result);
      }).catch((err) => {
        sendFailure(err);
      });
    } else {
      sendSuccess(value);
    }
  } catch (err) {
    sendFailure(err);
  }
});
