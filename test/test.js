var assert = require('chai').assert;
var sfdx = require('../index');
var sfdxParallel = require('../parallel');

describe('sfdx', function () {
  it('should have object \'org\'', function () {
    assert.isObject(sfdx.force.org);
  })
  describe('org', function () {
    it('should have function \'list\'', function () {
      assert.isFunction(sfdx.force.org.list);
    });
    it('should have function \'open\'', function () {
      assert.isFunction(sfdx.force.org.open);
    });
  })
});

describe('sfdx parallel', function () {
  it('should have object \'org\'', function () {
    assert.isObject(sfdxParallel.force.org);
  })
  describe('org', function () {
    it('should have function \'list\'', function () {
      assert.isFunction(sfdxParallel.force.org.list);
    });
    it('should have function \'open\'', function () {
      assert.isFunction(sfdxParallel.force.org.open);
    });
  })
});

describe('sfdx command execution', function () {
  this.timeout(200000);
  it('should resolve even when trying to display a non-existing org', function (done) {
    sfdx.force.org.display({
      targetusername: 'wrong@username',
      _quiet: false,
    })
      .then((displayResults) => {
        assert.isUndefined(displayResults);
        done();
      })
      .catch(() => {
        done(new Error('Expected method call to resolve.'))
      });
  });
  it('should reject when trying to display a non-existing org', (done) => {
    sfdx.force.org.display({
      targetusername: 'wrong@username',
      _quiet: false,
      _rejectOnError: true,
    })
      .then(() => {
        done(new Error('Expected method call to reject.'))
      })
      .catch((err) => {
        assert.isArray(err);
        assert.include(err[0].message, 'wrong@username', 'Did not receive expected error from the method call.');
        done();
      });
  });
});

describe('sfdx parallel command execution', function () {
  this.timeout(200000);
  it('With parallel execution, only the calls with an error should fail, others should pass without any impact on them', (done) => {
    const x1 = sfdxParallel.force.org.display({
      targetusername: 'wrong@username',
      _quiet: false,
      _rejectOnError: true,
    })
      .then(() => {
        throw new Error('Expected first method call to reject.');
      })
      .catch((err1) => {
        if (err1 instanceof Error && err1.message === 'Expected first method call to reject.') {
          throw err1;
        }
        assert.isArray(err1);
        return err1[0].message;
      });

    const x2 = sfdxParallel.config.list({
      _quiet: false,
      _rejectOnError: true,
    })
      .then((listResult) => {
        return listResult;
      })
      .catch((err2) => {
        throw new Error('Expected second method call to resolve.');
      });

    Promise.all([x1, x2])
      .then(([x1Result, x2Result]) => {
        assert.include(x1Result, 'wrong@username', 'Did not receive expected error from first method call.');
        assert.isArray(x2Result);
        done();
      })
      .catch((err) => {
        done(err);
      });
  });
});
