const { fork } = require('child_process');
const path = require('path');
const _ = require('lodash');
const Promise = require('bluebird');
const { getAllSFDXCommands } = require('./lib/read-plugins');

const sfdxApi = {};

const _createCommand = (commandPath) => (flags, opts) => new Promise((resolve, reject) => {
  const childArgs = {
    commandPath,
    flags,
    opts
  };
  const child = fork(path.join(__dirname, 'lib', 'child.js'), ['--colors'], { stdio: 'pipe' });
  child.stderr.on('data', (data) => {
    console.log(data.toString().replace(new RegExp('\\n$'), ''));
  });
  child.stdout.on('data', (data) => {
    console.log(data.toString().replace(new RegExp('\\n$'), ''));
  });
  child.on('message', (message) => {
    if (message.type === 'resolved') {
      resolve(message.value);
    } else {
      reject(message.value);
    }
    // Kill any lingering child processes once it has resolved or rejected
    try {
      // Don't kill the child process when an Org is being opened
      // On windows it takes time to open the browser even after resolving or rejecting
      if (commandPath !== 'force.org.open') {
        child.kill();
      }
    } catch (e) {
      // Ignore
    }
  });
  child.send({
    cmd: "SFDX_PARALLEL_init",
    args: childArgs
  });
});

const buildAllCommands = () => {
  const commands = getAllSFDXCommands();
  _.forEach(commands, (cmdObj) => {
    const { namespace, topic, methodName } = cmdObj;
    let commandPath;
    if (methodName) {
      commandPath = `${namespace}.${topic}.${methodName}`;
    } else {
      commandPath = `${namespace}.${topic}`;
    }
    _.set(sfdxApi, commandPath, _createCommand(commandPath));
  });
};

buildAllCommands();
module.exports = sfdxApi;
